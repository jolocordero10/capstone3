import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Home(){

	const data = {
	    title: "Justine's Online Shop",
	    content: "Everything is free except for me.",
	    destination: "/products",
	    label: "Shop now!"
	}

	return(
		<>
			<Banner data={data}/>
			<Highlights/>
		</>
	)
}