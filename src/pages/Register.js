import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const { user } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	function registerUser(event){

		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/api/user/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true) {
				Swal.fire({
				    title: "Duplicate email found.",
				    icon: "error",
				    text: "Use another email to register."
				})

			} else {
				
				fetch(`${process.env.REACT_APP_API_URL}/api/user/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email: email,
						password: password
					})
				}).then(response => response.json()).then(result => {
					if(result){
						setEmail("")
						setPassword("")
						setConfirmPassword("")

						Swal.fire({
						    title: "Registration successful",
						    icon: "success",
						    text: "You may now log in to your account."
						})
					} else {
						Swal.fire({
						    title: "Registration failed",
						    icon: "error",
						    text: "Something went wrong, please try again."
						})
					}
				})
			}
		})
	}

	useEffect(() => {
		if((email !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password, confirmPassword]);

	return(
		(user.id !== null) ?
			<Navigate to="/products" />
		:
				<Form onSubmit={(event) => registerUser(event)}>
			        <h1 className="my-5 text-center">Register</h1>
			            <Form.Group>
			                <Form.Label>Email:</Form.Label>
			                <Form.Control 
			                	type="email" 
			                	placeholder="Enter Email" 
			                	required
			                	value={email}
			                	onChange={event => {setEmail(event.target.value)}}
			                />
			            </Form.Group>
			            <Form.Group>
			                <Form.Label>Password:</Form.Label>
			                <Form.Control 
			                	type="password" 
			                	placeholder="Enter Password" 
			                	required
			                	value={password}
			                	onChange={event => {setPassword(event.target.value)}}
			                />
			            </Form.Group>
			            <Form.Group>
			                <Form.Label>Confirm Password:</Form.Label>
			                <Form.Control 
			                	type="password" 
			                	placeholder="Confirm Password" 
			                	required
			                	value={confirmPassword}
			                	onChange={event => {setConfirmPassword(event.target.value)}}
			                />
			            </Form.Group>

			            <Button className="mt-3" variant="primary" type="submit" disabled={isActive === false}>Submit</Button>          
			       </Form>
	)
}