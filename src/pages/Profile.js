import {useContext, useState, useEffect} from 'react';
import {Row, Col} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';



export default function Profile(){

    const {user} = useContext(UserContext);

    const [details,setDetails] = useState({})

    useEffect(()=>{

        fetch(`${process.env.REACT_APP_API_URL}/api/user/details`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (typeof data._id !== "undefined") {

                setDetails(data);

            }
        });

    },[])


    return (
        (user.id === null) ?
        <Navigate to="/" />
        :
        <>
            <Row>
                <Col className="p-5 bg-primary text-white">
                    <h1 className="my-5 ">Profile</h1>
                    <h2 className="mt-3">Email: {details.email}</h2>
                </Col>
            </Row>
        </>

    )

}
