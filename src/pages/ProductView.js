import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const { user } = useContext(UserContext)

	const { productId } = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);

	const checkout = (productId, quantity) => {
		fetch(`${process.env.REACT_APP_API_URL}/api/user/checkout`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
				    title: "Checkout Successful",
				    icon: "success",
				    text: "You have successfully completed the checkout process."
				})

				navigate("/products");

			} else {
				Swal.fire({
				    title: "Checkout Failed",
				    icon: "error",
				    text: "Something went wrong. Please try again."
				})
			}
		})
	};


	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/api/product/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})

	}, [productId]);

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text> 

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PHP{price}</Card.Text>

							<Card.Text>Quantity:</Card.Text>
							<input
							    type="number"
							    value={quantity}
							    min="1"
							    onChange={(e) => setQuantity(e.target.value)}
							/>
							<div className="mt-3">
							{
								(user.id !== null || user.isAdmin === false) ?
									<Button variant="primary" onClick={() => checkout(productId, quantity)}>Buy</Button>
									:
									<Link className="btn btn-danger" to="/login">Login as User to buy the product!</Link>
							}
						    </div>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}